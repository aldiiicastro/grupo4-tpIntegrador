package zona;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import inspector.Inspector;
import puntoVenta.PuntoDeVenta;

class ColaboradorZonaTest {
	private EncargadoGuardarZona cZona;
	private PuntoDeVenta punto;
	private List<PuntoDeVenta> puntos;
	private Inspector inspector;
	
	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		punto = mock(PuntoDeVenta.class);
		puntos = new ArrayList<PuntoDeVenta>();
		puntos.add(punto);
		inspector = mock(Inspector.class);
		
		//SUT
		cZona = new EncargadoGuardarZona();
	}
	
	@Test
	void test() {
		cZona.registrar(puntos, inspector);
		assertEquals(1, cZona.getZonas().size());
	}

	@Test
	void testPrecioXHora() {
		assertEquals(40, cZona.getPrecioXHora());
	}
}
