package zona;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import inspector.Inspector;
import puntoVenta.PuntoDeVenta;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

class ZonaTest {
	private Zona zona;
	private PuntoDeVenta punto;
	private List<PuntoDeVenta> puntos;
	private Inspector inspector;
	
	@BeforeEach 
	public void setUp() throws Exception {
		punto = mock(PuntoDeVenta.class);
		puntos = new ArrayList<PuntoDeVenta>();
		puntos.add(punto);
		inspector = mock(Inspector.class);
		zona = new Zona(puntos, inspector);
	}
	
	@Test
	void testTieneInspector() {
		assertEquals(inspector, zona.getInspector());
	}
	
	@Test
	void testTienePuntoDeVenta() {
		assertEquals(punto, zona.getPunto().get(0));
		assertEquals(1, zona.getPunto().size());
	}
	
	@Test
	void testTieneDosPuntosDeVenta() {
		PuntoDeVenta punto1 = mock(PuntoDeVenta.class);
		puntos.add(punto1);
		Zona zona1 =  new Zona(puntos, inspector);
		assertEquals(punto, zona1.getPunto().get(0));
		assertEquals(punto1, zona1.getPunto().get(1));
		assertEquals(2, zona1.getPunto().size());
	}
	
	@Test
	void testCualquierPunto() {
		assertEquals(punto, zona.obtenerCualquierPunto());
	}
}