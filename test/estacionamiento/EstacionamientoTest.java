package estacionamiento;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EstacionamientoTest {
	private Estacionamiento estacionamiento;
	private LocalTime horaFin ;
	@BeforeEach 
	public void setUp() throws Exception {
		LocalTime horaInicio = LocalTime.of(10,0,0);
		horaFin = LocalTime.of(16,0,0);
		estacionamiento = new Estacionamiento("EDB949", horaInicio, horaFin,1139538872);
	}		
	@Test
	void testGetPatente() {
		assertEquals("EDB949", estacionamiento.getPatente());
	}
	
	@Test
	void testGetHoraInicio() {
		LocalTime horaInicio = LocalTime.of(10,0,0);
		assertEquals(horaInicio, estacionamiento.getHoraInicio());
	}
	@Test
	void testSetFin() {
		assertEquals(horaFin, estacionamiento.getHoraFin());
		LocalTime finalizo = LocalTime.of(13,30,0);
		estacionamiento.finDeEstacionamiento(finalizo);
		assertEquals(finalizo, estacionamiento.getHoraFin());
	}
	
	@Test
	void testEstaVigente() {
		LocalTime horaPedido = LocalTime.of(15,0,0);
		assertTrue(estacionamiento.vigencia(horaPedido));
	}
	
	@Test
	void testNoEstaVigente() {
		LocalTime horaPedido = LocalTime.of(18,0,0);
		assertFalse(estacionamiento.vigencia(horaPedido));
	}

}
