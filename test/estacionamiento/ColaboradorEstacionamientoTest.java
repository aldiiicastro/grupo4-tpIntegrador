package estacionamiento;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import monitoreoEstacionamiento.EncargadoGestionarEmpresa;

import java.time.LocalTime;

class ColaboradorEstacionamientoTest {
	private EncargadoGuardarEstacionamiento colEst;
    private EncargadoGestionarEmpresa encargadoGestionarEmpresa;
	private LocalTime horaFin;

	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		encargadoGestionarEmpresa = mock(EncargadoGestionarEmpresa.class);
		
		//SUT
		colEst = new EncargadoGuardarEstacionamiento();
		LocalTime horaInicio = LocalTime.of(8,0,0);
		horaFin =  horaInicio.plusHours(4);
		colEst.registrarEstacionamiento(encargadoGestionarEmpresa, "EDB949", horaInicio, horaFin, 1139538872);
		colEst.registrarEstacionamiento(encargadoGestionarEmpresa, "EIZ789", horaInicio, horaFin, 40);
	}
	
	@Test
	void testRegistrarEstacionamiento() {		
		assertEquals(2, colEst.getEstacionamientos().size());
	}
	
	@Test
	void testEstaVigente() {
		LocalTime ahora = LocalTime.of(9, 30, 0);
		assertTrue(colEst.estaVigente("EDB949", ahora));
		assertTrue(colEst.estaVigente("EIZ789", ahora));
	}
	
	@Test
	void testNoEstaVigente() {
		LocalTime ahora =LocalTime.of(12, 30, 0);
		assertFalse(colEst.estaVigente("EDB949", ahora));
		assertFalse(colEst.estaVigente("EIZ789", ahora));
	}
	
	@Test
	void testCambiarHoraFin(){
		LocalTime finalizado = LocalTime.of(15, 30,0);
		colEst.finDeEstacionamiento(encargadoGestionarEmpresa,finalizado,1139538872);
		assertEquals(finalizado, colEst.getEstacionamientos().get(0).getHoraFin());
	}
}
