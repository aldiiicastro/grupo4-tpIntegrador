package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EstadoManejandoTest {
	EstadoManejando estado;
	AppUsuarioAutomatica app;
	
	@BeforeEach
	void setUp() throws Exception {
		app = mock(AppUsuarioAutomatica.class);
		
		estado = new EstadoManejando(1139538872);
	}
	@Test
	void testGetCelular() {
		assertEquals(1139538872, estado.getCelular());
	}
	
	@Test
	void testCambiarAManejando() {
		estado.cambiarAManejando(app);
		verify(app, times(1)).setEstado(estado);
	}

}
