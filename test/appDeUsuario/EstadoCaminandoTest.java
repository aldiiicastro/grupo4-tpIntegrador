package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EstadoCaminandoTest {
	EstadoCaminando estado = new EstadoCaminando("EDB949", 1139538872);
	@Test
	void testPatente() {
		assertEquals("EDB949", estado.getPatente());
	}
	
	@Test
	void testCelular() {
		assertEquals(1139538872, estado.getCelular());
	}

}
