package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sem.SEM;

class AppUsuarioTest {
	private AppUsuario app;
	private SEM sem;
	private LocalTime ahora = LocalTime.now();
	
	@BeforeEach 
	void setUp() throws Exception {
		//DOC
		sem = mock(SEM.class);
		//SUT
		app = new AppUsuario(1139538872, "EDB949", sem);
	}
	
	@Test
	void testIniciarManual() {
		when(sem.iniciarEstacionamineto(ahora, "EDB949", 1139538872)).thenReturn("Funciono manual");
		assertEquals("Funciono manual", app.iniciarEstacionamiento(ahora));
	}
	
	@Test
	void testFinManual() {
		when(sem.finDeEstacionamiento(ahora, 1139538872)).thenReturn("Funciono manual");
		assertEquals("Funciono manual", app.finDeEstacionamiento(ahora));
	}

	@Test
	void testNotificacionesManual() {
		assertEquals("", app.getNotificaciones());
		app.getStrategy().driving();
		assertEquals("Esta manejando, no se olvide de finalizar el estacionamiento", app.getNotificaciones());
		app.getStrategy().walking();
		assertEquals("Esta caminando, no se olvide de inicializar el estacionamiento", app.getNotificaciones());
	}
	
	@Test
	void testIniciarAutomaticoCaminando() {
		app.modoAutomatico();
		when(sem.iniciarEstacionamineto(ahora, "EDB949", 1139538872)).thenReturn("Funciono Automatico");
		assertEquals("Funciono Automatico", app.iniciarEstacionamiento(ahora));
	}
	
	@Test
	void testFinAutomaticoCaminando() {
		app.modoAutomatico();
		assertEquals("Esta caminando, no se puede finalizar", app.finDeEstacionamiento(ahora));
	}
	
	@Test
	void testFinAutomaticoManejando() {
		app.modoAutomatico();
		app.getStrategy().driving();
		when(sem.finDeEstacionamiento(ahora, 1139538872)).thenReturn("Funciono Automatico");
		assertEquals("Funciono Automatico", app.finDeEstacionamiento(ahora));
	}
	
	@Test
	void testInicioAutomaticoManejando() {
		app.modoAutomatico();
		app.getStrategy().driving();
		assertEquals("Esta manejando no se puede inicializar", app.iniciarEstacionamiento(ahora));
	}
	@Test
	void testNotificacionesAutomatico() {
		app.modoAutomatico();
		assertEquals("", app.getNotificaciones());
		app.getStrategy().driving();
		assertEquals("Esta manejando, automaticamente se finaliza el estacionamiento", app.getNotificaciones());
		app.getStrategy().walking();
		assertEquals("Esta caminando, automaticamente se inicializa el estacionamiento", app.getNotificaciones());
	}
	
	@Test
	void testGetPatente() {
		assertEquals("EDB949", app.obtenerPatente());
	}
	
	@Test
	void testNumeroDeCelular() {
		assertEquals(1139538872, app.nroCelular());
	}
	
	@Test
	void consultarSaldo() {
		when(sem.consultarSaldo(1139538872)).thenReturn(50);
		assertEquals(50, app.consultaSaldo(1139538872));
	}
	
	@Test
	void testModoAutomaticoyGetStrategy() {
		AppUsuarioAutomatica auto = new AppUsuarioAutomatica(1139538872, "EDB949", sem);
		app.modoAutomatico();
		assertEquals(auto.getClass(), app.getStrategy().getClass());
	}
	
	@Test
	void testModoManualyGetStrategy() {
		AppUsuarioManual manual = new AppUsuarioManual(1139538872, "EDB949", sem);
		app.modoManual();
		assertEquals(manual.getClass(), app.getStrategy().getClass());
	}
}
