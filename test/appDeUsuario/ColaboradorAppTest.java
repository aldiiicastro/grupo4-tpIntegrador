package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import puntoVenta.EncargadoGuardarCredito;
import sem.SEM;
import zona.EncargadoGuardarZona;

class ColaboradorAppTest {
	private EncargadoAdminstrarApp colApp;
	private SEM sem;
	private EncargadoGuardarCredito colCre;
	private LocalTime horaInicio, inicio, fin;
	private int precioXHora;
	private String response;
	
	@BeforeEach	
	void setUp() throws Exception {
		//DOC
		sem = mock(SEM.class);
		colCre = mock(EncargadoGuardarCredito.class);
		//SET UP
		horaInicio = LocalTime.of(9, 30, 0);
		EncargadoGuardarZona colZona = new EncargadoGuardarZona();
		precioXHora = colZona.getPrecioXHora();
		inicio = colZona.getInicioFranja();
		fin = colZona.getFinFranja();
		
		//SUT
		colApp = new EncargadoAdminstrarApp(precioXHora, inicio, fin, colCre, sem);
	}
	
	@Test
	void testIniciarEstacionamineto() {
			//Mock para credito > 40
			when(colCre.cuantoCreditoTiene(1139538872)).thenReturn(83);
			//Me guardo la respuesta
			response = colApp.iniciarEstacionamineto("EDB949", 1139538872, horaInicio);
			//Calculo la hora fin para el expected
			LocalTime horaFin = horaInicio.plusHours(colCre.cuantoCreditoTiene(1139538872)/precioXHora) ;
			//Verifico que se llame al registrar
			verify(sem, times(1)).registrarEstacionamiento("EDB949", horaInicio, horaFin, 1139538872);
			//Creo lo que quiero esperar y le hago el assert
			String expected = "Arranca estacionamiento a las: " + horaInicio + " tiene tiempo hasta las: " +horaFin;
			assertEquals(expected, response);
	}
	
	@Test
	void testNoIniciarEstacionamineto() {
		//Mock para credito < 40
		when(colCre.cuantoCreditoTiene(1139538872)).thenReturn(39);
		//Me guardo la respuesta
		response = colApp.iniciarEstacionamineto("EDB949", 1139538872, horaInicio);
		//Creo lo que quiero esperar y le hago el assert
		String expected = "Saldo insuficiente. Estacionamiento no permitido";
		assertEquals(expected, response); 
	}
	
	@Test
	void testNoEsHoraDeCobro() {
		horaInicio = LocalTime.of(20, 30, 0);
		//Mock para credito < 40
		when(colCre.cuantoCreditoTiene(1139538872)).thenReturn(50);
		//Me guardo la respuesta
		response = colApp.iniciarEstacionamineto("EDB949", 1139538872, horaInicio);
		//Creo lo que quiero esperar y le hago el assert
		String expected = "No esta dentro del horario de cobro";
		assertEquals(expected, response); 
	}
	@Test 
	void testFinDeEstacionamiento() {
		//Iniico el estacionamiento
		colApp.iniciarEstacionamineto("EDB949", 1139538872, horaInicio);
		//Me guardo la hora actual para poder saber cuando se finalizo
		LocalTime horaActual = LocalTime.now();
		LocalTime finalizar = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond());
		//Calculo la cantidad de horas para el expected y el costo
		int totalDeHoras = finalizar.minusHours(horaInicio.getHour()).getHour();
		int costo = totalDeHoras * precioXHora;
		//Creo lo que quiero esperar y le hago el assert y verifo que se llame a debitar
		String expected = "Comenzo a las: " + horaInicio + "finalizo a las: " + finalizar + ", un total de: " + totalDeHoras + " horas. Costo: " + costo;
		assertEquals(expected, colApp.finDeEstacionamiento(finalizar, 1139538872));
		verify(colCre, times(1)).debitarCredito(costo, 1139538872);
	}
}
