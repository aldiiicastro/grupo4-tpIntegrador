package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sem.SEM;

class AppUsuarioManualTest {
	private AppUsuarioManual app;
	private SEM sem;
	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		sem = mock(SEM.class);
		
		//SUT
		app = new AppUsuarioManual(1139538872, "EDB949", sem);
	}
	
	@Test
	void testIniciarEstacionamiento() {
		LocalTime ahora = LocalTime.now();
		when(sem.iniciarEstacionamineto(ahora, "EDB949", 1139538872)).thenReturn("Funciona");
		assertEquals("Funciona", app.iniciarEstacionamiento(ahora));
		verify(sem, times(1)).iniciarEstacionamineto(ahora, "EDB949", 1139538872);
	}
	
	@Test
	void testFinalizarEstacionamiento() {
		LocalTime ahora = LocalTime.now();
		when(sem.finDeEstacionamiento(ahora, 1139538872)).thenReturn("Funciona");
		assertEquals("Funciona", app.finDeEstacionamiento(ahora));
		verify(sem, times(1)).finDeEstacionamiento(ahora, 1139538872);
	}

	@Test
	void testConsultarSaldo() {
		when(sem.consultarSaldo(1139538872)).thenReturn(50);
		assertEquals(50, app.consultaSaldo(1139538872));
		verify(sem, times(1)).consultarSaldo(1139538872);
	}
	
	@Test
	void testDriving() {
		app.driving();
		assertEquals("Esta manejando, no se olvide de finalizar el estacionamiento", app.notificaciones());
	}
	
	@Test
	void testWalking() {
		app.walking();
		assertEquals("Esta caminando, no se olvide de inicializar el estacionamiento", app.notificaciones());
	}
	
	@Test
	void testGetPatente() {
		assertEquals("EDB949", app.obtenerPatente());
	}
	
	@Test
	void testNumeroDeCelular() {
		assertEquals(1139538872, app.nroCelular());
	}
	
	@Test
	void consultarSaldo() {
		when(sem.consultarSaldo(1139538872)).thenReturn(50);
		assertEquals(50, app.consultaSaldo(1139538872));
	}
}
