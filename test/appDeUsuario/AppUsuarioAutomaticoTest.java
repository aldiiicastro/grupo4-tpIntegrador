package appDeUsuario;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sem.SEM;

class AppUsuarioAutomaticoTest {
	private AppUsuarioAutomatica app;
	private SEM sem;
	private Estado estado;
	private LocalTime ahora = LocalTime.now();
	private EstadoCaminando caminando;
	private EstadoManejando manejando;
	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		sem = mock(SEM.class);
		estado = mock(Estado.class);
		manejando = new EstadoManejando(1139538872);
		caminando = new EstadoCaminando("EDB949", 1139538872);
		//SUT
		app = new AppUsuarioAutomatica(1139538872, "EDB949", sem);
	}
	
	//En estos 4 test se evalua que segun el cambio de estado de lo que corresponde
	@Test
	void testIniciarCaminando() {
		when(sem.iniciarEstacionamineto(ahora, "EDB949", 1139538872)).thenReturn("Funciono");
		assertEquals("Funciono", app.iniciarEstacionamiento(ahora));
		assertEquals(caminando.getClass(), app.getEstado().getClass());
	}
	
	@Test
	void testIniciarManejando() {
		app.setEstado(manejando);
		assertEquals("Esta manejando no se puede inicializar", app.iniciarEstacionamiento(ahora));
		assertEquals(manejando.getClass(), app.getEstado().getClass());
	}
	
	@Test
	void testFinalizarEnManejando() {
		app.setEstado(manejando);
		when(sem.finDeEstacionamiento(ahora, 1139538872)).thenReturn("Funciono");
		assertEquals("Funciono", app.finDeEstacionamiento(ahora));
		assertEquals(manejando.getClass(), app.getEstado().getClass());
	}
	
	@Test 
	void testFinalizarEnCaminando() {
		assertEquals("Esta caminando, no se puede finalizar", app.finDeEstacionamiento(ahora));
		assertEquals(caminando.getClass(), app.getEstado().getClass());
	}
	
	//En estos dos test se evalua sin importar el estado, que al estado le lleguen los mensajes
	@Test
	void testIniciar() {
		app.setEstado(estado);
		app.iniciarEstacionamiento(ahora);
		verify(estado, times(1)).caminando(sem, ahora);
		verify(estado, times(1)).getMsg();
	}

	@Test
	void testFin() {
		app.setEstado(estado);
		app.finDeEstacionamiento(ahora);
		verify(estado, times(1)).manejando(sem, ahora);
		verify(estado, times(1)).getMsg();
	}
	
	//Resto de test
	@Test 
	void testDriving() {
		app.driving();
		assertSame(manejando.getClass(), app.getEstado().getClass());
		assertEquals("Esta manejando, automaticamente se finaliza el estacionamiento", app.notificaciones());
	}
	
	@Test
	void testWalking() {
		app.walking();
		assertSame(caminando.getClass(), app.getEstado().getClass());
		assertEquals("Esta caminando, automaticamente se inicializa el estacionamiento", app.notificaciones());
	}
	
	@Test
	void testNotificaciones() {
		assertEquals("", app.notificaciones());
	}
	
	@Test
	void testGetPatente() {
		assertEquals("EDB949", app.obtenerPatente());
	}
	
	@Test
	void testGetNumeroControl() {
		assertEquals(1139538872, app.nroCelular());
	}
	
	@Test
	void consultarSaldo() {
		when(sem.consultarSaldo(1139538872)).thenReturn(50);
		assertEquals(50, app.consultaSaldo(1139538872));
	}
}
