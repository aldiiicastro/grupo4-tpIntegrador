package sem;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import appDeUsuario.EncargadoAdminstrarApp;
import estacionamiento.EncargadoGuardarEstacionamiento;
import estacionamiento.Estacionamiento;
import inspector.EncargadoGuardarInfraccion;
import inspector.Inspector;
import monitoreoEstacionamiento.Alerta;
import monitoreoEstacionamiento.EncargadoGestionarEmpresa;
import monitoreoEstacionamiento.IEmpresa;
import puntoVenta.EncargadoGuardarCredito;
import puntoVenta.PuntoDeVenta;
import registroCompra.EncargadoRegistrarCompra;
import zona.EncargadoGuardarZona;

class SEMTest {
	private EncargadoGuardarInfraccion colaboradorDeInfraccion;
	private EncargadoGuardarCredito colaboradorCredito;
	private EncargadoGuardarEstacionamiento colaboradorEstacionamiento;
	private EncargadoAdminstrarApp colaboradorApp;
	private EncargadoGuardarZona colaboradorZona;
	private EncargadoRegistrarCompra colaboradorRegistroCompra;
	private EncargadoGestionarEmpresa encargadoGestionarEmpresa;
	private SEM sem;
	 
	private IEmpresa empresa1;
	private IEmpresa empresa2;
	private	IEmpresa empresa3; 
	
	@BeforeEach
	void setUp() throws Exception {
		//DOC
		colaboradorDeInfraccion = mock(EncargadoGuardarInfraccion.class);
		colaboradorCredito = mock(EncargadoGuardarCredito.class);
		colaboradorEstacionamiento = mock(EncargadoGuardarEstacionamiento.class);
		colaboradorApp = mock(EncargadoAdminstrarApp.class);
		colaboradorZona = mock(EncargadoGuardarZona.class);
		colaboradorRegistroCompra = mock(EncargadoRegistrarCompra.class);
		encargadoGestionarEmpresa = mock(EncargadoGestionarEmpresa.class);
		empresa1 = mock(IEmpresa.class);
		empresa2 = mock(IEmpresa.class);
		empresa3 = mock(IEmpresa.class);

		//SUT
		sem = new SEM(colaboradorDeInfraccion, colaboradorCredito, colaboradorEstacionamiento, colaboradorApp, colaboradorZona, colaboradorRegistroCompra, encargadoGestionarEmpresa);
		}
	
	@Test
	void testrRegistrarZona() {
		List<PuntoDeVenta> puntos = new ArrayList<PuntoDeVenta>();
		Inspector inspector = mock(Inspector.class);
		sem.registrarZona(puntos, inspector);
		verify(colaboradorZona, times(1)).registrar(puntos, inspector);
	}
	
	@Test
	void testRegistrarCompraEstacionamiento() {
		LocalTime horaActual = LocalTime.now();
		LocalDate fechaActual = LocalDate.now();
		PuntoDeVenta puntoDeVenta = mock(PuntoDeVenta.class);
		sem.registrarCompraEstacionamiento("EDB949", 3, fechaActual, horaActual, puntoDeVenta);
		verify(colaboradorRegistroCompra, times(1)).registrarCompraEstacionamiento("EDB949", 3, fechaActual, horaActual, puntoDeVenta);
	}
	@Test
	void testRegistrarEstacionamiento() {
		LocalTime horaInicio = LocalTime.of(10,0,0);
		LocalTime fin = LocalTime.of(16,0,0);
		sem.registrarEstacionamiento("EDB949", horaInicio, fin, 1139538872);
		verify(colaboradorEstacionamiento, times(1)).registrarEstacionamiento(encargadoGestionarEmpresa, "EDB949", horaInicio, fin, 1139538872);
	}
	@Test
	void testRegistrarCredito() {
		LocalTime horaActual = LocalTime.now();
		LocalDate fechaActual = LocalDate.now();
		PuntoDeVenta punto = mock(PuntoDeVenta.class);
		sem.registrarCredito(1139538872, 500, fechaActual, horaActual, punto);
		verify(colaboradorRegistroCompra, times(1)).registrarCompraCredito(encargadoGestionarEmpresa, 1139538872, 500,fechaActual, horaActual, punto);
		verify(colaboradorCredito, times(1)).registrarCredito(1139538872, 500);
	}
	@Test
	void testRegistrarInfraccion() {
		Inspector inspector = mock(Inspector.class);
		LocalTime soloHora = LocalTime.of(10,0,0);
		LocalDate soloFecha = LocalDate.now();
		sem.registrarInfraccion("EDB949", inspector, soloHora, soloFecha);
		verify(colaboradorDeInfraccion, times(1)).guardarInfraccion("EDB949", inspector, soloHora, soloFecha);
	}
			
	@Test
	void testFinDeFranjaHoraria() {
		//Se hace un spy del sem para poder ver si se llama o no a fin de estacionamiento
		SEM spySem = Mockito.spy(sem);
		//Se crea una lista para que el colaborador devuelva y poder ver si entra al if
		ArrayList<Estacionamiento> est = new ArrayList<Estacionamiento>();
		Estacionamiento e = mock(Estacionamiento.class);
		est.add(e);
		when(colaboradorEstacionamiento.getEstacionamientos()).thenReturn(est);
		
		//Fin de la franja 
		LocalTime fin = LocalTime.of(20, 0, 0);
		//Se le dice a zona que retornar
		
		when(colaboradorZona.getFinFranja()).thenReturn(fin);
		//Estacionamiento que si esta vigente
		when(e.vigencia(fin)).thenReturn(true);
		
		//Que se llego dentro del if
		assertEquals("Termino el horario", spySem.finDeFranjaHoraria(fin));
		
		//Se verifica que se llamen
		verify(spySem, times(1)).finDeEstacionamiento(fin, 0);
		verify(colaboradorEstacionamiento, times(1)).getEstacionamientos();
	}
	
	@Test
	void testElEstacionamientoNoEstaVigenteFinDeFranjaHoraria() {
		ArrayList<Estacionamiento> est = new ArrayList<Estacionamiento>();
		Estacionamiento e = mock(Estacionamiento.class);
		est.add(e);
		when(colaboradorEstacionamiento.getEstacionamientos()).thenReturn(est);
		LocalTime fin = LocalTime.of(20, 0, 0);
		when(colaboradorZona.getFinFranja()).thenReturn(fin);
		when(e.vigencia(fin)).thenReturn(false);
		assertEquals("Termino el horario", sem.finDeFranjaHoraria(fin));
		verify(colaboradorEstacionamiento, times(1)).getEstacionamientos();
	}
	
	@Test
	void testNotFinDeFranjaHoraria() {
		LocalTime fin = LocalTime.of(20, 0, 0);
		when(colaboradorZona.getFinFranja()).thenReturn(fin);
		assertEquals("Aun no termino el horario", sem.finDeFranjaHoraria(LocalTime.of(19, 30, 40)));
		verify(colaboradorEstacionamiento, times(1)).getEstacionamientos();
	}
	
	@Test
	void testIniciarEstacionamineto() {
		LocalTime horaActual = LocalTime.now();
		when(colaboradorApp.iniciarEstacionamineto("EDB949",1139538872, horaActual)).thenReturn("Inicio estacionamiento");
		assertEquals("Inicio estacionamiento", sem.iniciarEstacionamineto(horaActual, "EDB949", 1139538872));
		verify(colaboradorApp, times(1)).iniciarEstacionamineto("EDB949",1139538872, horaActual);
	}
	@Test
	void testFinDeEstacionamiento() {
		LocalTime horaActual = LocalTime.now();
		when(colaboradorApp.finDeEstacionamiento(horaActual, 1139538872)).thenReturn("Fin de estacionamiento");
		assertEquals("Fin de estacionamiento", sem.finDeEstacionamiento(horaActual, 1139538872));
		verify(colaboradorEstacionamiento, times(1)).finDeEstacionamiento(encargadoGestionarEmpresa, horaActual, 1139538872);
		verify(colaboradorApp, times(1)).finDeEstacionamiento(horaActual, 1139538872);
	}
	@Test
	void testConsultarSaldo() {
		when(colaboradorCredito.cuantoCreditoTiene(1139538872)).thenReturn(100);
		assertEquals(100, sem.consultarSaldo(1139538872));
		verify(colaboradorCredito, times(1)).cuantoCreditoTiene(1139538872);
	}
	
	@Test
	void TestEstaVigente() {
		LocalTime horaActual = LocalTime.now();
		when(colaboradorEstacionamiento.estaVigente("EDB949", horaActual)).thenReturn(true);
		assertTrue(sem.estaVigente("EDB949", horaActual));
		verify(colaboradorEstacionamiento, times(1)).estaVigente("EDB949", horaActual);
	}
	
	@Test
	void TestNoEstaVigente() {
		LocalTime horaActual = LocalTime.now();
		when(colaboradorEstacionamiento.estaVigente("EDB949", horaActual)).thenReturn(false);
		assertFalse(sem.estaVigente("EDB949", horaActual));
		verify(colaboradorEstacionamiento, times(1)).estaVigente("EDB949", horaActual);
	}
	
	@Test
	void testSuscripcion() {
		Set<IEmpresa> empresas = new HashSet<IEmpresa>();
		empresas.add(empresa1);
		empresas.add(empresa2);
		empresas.add(empresa3);
		sem.suscribirEmpresa(empresa1);
		sem.suscribirEmpresa(empresa2);
		sem.suscribirEmpresa(empresa3);
		when(encargadoGestionarEmpresa.getEmpresasSuscriptas()).thenReturn(empresas);
		assertEquals(3, sem.getEmpresasSuscriptas().size());
	}

	@Test
	void testDesuscripcion() {
		Set<IEmpresa> empresas = new HashSet<IEmpresa>();
		empresas.add(empresa1);
		empresas.add(empresa2);
		empresas.add(empresa3);
		Alerta alerta = mock(Alerta.class);
		sem.desuscribirEmpresa(empresa3, alerta);
		empresas.remove(empresa3);
		when(encargadoGestionarEmpresa.getEmpresasSuscriptas()).thenReturn(empresas);
		assertEquals(2, sem.getEmpresasSuscriptas().size());
		verify(encargadoGestionarEmpresa, times(1)).desuscribirEmpresa(empresa3, alerta);
	}
	
	@Test
	void testDarDeBaja() {
		Set<IEmpresa> empresas = new HashSet<IEmpresa>();
		empresas.add(empresa1);
		empresas.add(empresa2);
		empresas.add(empresa3);
		sem.darDeBaja(empresa3);
		empresas.remove(empresa3);
		when(encargadoGestionarEmpresa.getEmpresasSuscriptas()).thenReturn(empresas);
		assertEquals(2, sem.getEmpresasSuscriptas().size());
		verify(encargadoGestionarEmpresa, times(1)).darDeBaja(empresa3);
	}
}
