package inspector;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sem.SEM;

class AppMunicipalTest {
	private AppMunicipal app;
	private SEM sem;
	private LocalDate soloFecha = LocalDate.now();
	private LocalTime horaActual = LocalTime.now();
	private LocalTime soloHora = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond()), ahora =LocalTime.of(12, 30, 0);;


	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		sem = mock(SEM.class);
	
		//SUT
		app = new AppMunicipal(sem);
		
	}
	@Test
	void testEstaVigente() {
		when(sem.estaVigente("EDB949", ahora)).thenReturn(true);
		assertTrue(app.estaVigente("EDB949", ahora));
	}
	
	@Test
	void testNoEstaVigente() {
		when(sem.estaVigente("EDB949", ahora)).thenReturn(false);
		assertFalse(app.estaVigente("EDB949", ahora));
	}
	
	@Test
	void testInfraccion() {
		Inspector inspector = mock(Inspector.class);
		app.infraccion("EDB949", inspector, soloHora, soloFecha);
		verify(sem, times(1)).registrarInfraccion("EDB949", inspector, soloHora, soloFecha);
	}
}
