package inspector;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import zona.Zona;


class InspectorTest {
	private Zona zona;
	private AppMunicipal app;
	private Inspector inspector;
	private LocalDate soloFecha = LocalDate.now();
	private LocalTime ahora =LocalTime.of(12, 30, 0);	;

	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		zona = mock(Zona.class);
		app = mock(AppMunicipal.class);
		
		//SUT
		inspector = new Inspector(app);
		inspector.setZonaACargo(zona);
		
	}
	@Test
	void testZonaACargo() {
		assertEquals(zona, inspector.getZonaACargo());
	}
	
	@Test
	void testEstaVigenteEstacionamiento() {
		when(app.estaVigente("EDB949", ahora)).thenReturn(true);
		assertTrue(inspector.estaVigenteEstacionamiento("EDB949", ahora));
		verify(app, times(1)).estaVigente("EDB949", ahora);
		
	}
	
	@Test
	void testNoEstaVigenteEstacionamiento() {
		when(app.estaVigente("EDB949", ahora)).thenReturn(false);
		assertFalse(inspector.estaVigenteEstacionamiento("EDB949", ahora));
		verify(app, times(1)).estaVigente("EDB949", ahora);
	}
	
	@Test
	void testNoHayAltaDeInfraccion() {
		when(app.estaVigente("EDB949", ahora)).thenReturn(true);
		inspector.altaDeInfraccion("EDB949", ahora);
		verify(app, times(0)).infraccion("EDB949", inspector, ahora, soloFecha);
	}

	@Test
	void testAltaDeInfraccion() {
		when(app.estaVigente("EDB949", ahora)).thenReturn(false);
		inspector.altaDeInfraccion("EDB949", ahora);
		verify(app, times(1)).infraccion("EDB949", inspector, ahora, soloFecha);
	}
}
