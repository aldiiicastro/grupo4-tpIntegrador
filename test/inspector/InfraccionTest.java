package inspector;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import zona.Zona;

class InfraccionTest {
	private Infraccion infra;
	private Inspector insp;
	private LocalDate soloFecha;
	private LocalTime soloHora;
	private Zona zona;
	@BeforeEach
	void setUp() throws Exception {
		//DOC
		insp = mock(Inspector.class);
		zona = mock(Zona.class);
		when(insp.getZonaACargo()).thenReturn(zona);
		
		//Set  up
		soloFecha = LocalDate.now();
		soloHora = LocalTime.now();
		//SUT
		infra = new Infraccion("EDB949", insp, soloHora, soloFecha);
	}
	@Test
	void testGetInspector() {
		assertSame(insp, infra.getInspector());
	}
	@Test
	void testGetZona() {
		assertSame(zona, infra.getZona());
	}
	@Test
	void testGetPatente() {
		assertEquals("EDB949", infra.getPatente());
	}
	@Test
	void testGetFecha() {
		assertEquals(soloFecha, infra.getFecha());
	}
	@Test
	void testGetHora() {
		assertEquals(soloHora, infra.getHora());
	}
}
