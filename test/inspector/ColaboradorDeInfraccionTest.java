package inspector;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ColaboradorDeInfraccionTest {
	private EncargadoGuardarInfraccion colInf;
	private Inspector inspector;
	private LocalDate soloFecha = LocalDate.now();
	private LocalTime horaActual = LocalTime.now();
	private LocalTime soloHora = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond());

	
	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		inspector = mock(Inspector.class);
		
		//SUT
		colInf = new EncargadoGuardarInfraccion();
		
	}
	
	@Test
	void testSeGuardaLaInfraccion() {
		colInf.guardarInfraccion("EDB949", inspector, soloHora, soloFecha);
		assertEquals(1, colInf.getInfracciones().size());
	}
}
