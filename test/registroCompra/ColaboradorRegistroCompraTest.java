package registroCompra;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import monitoreoEstacionamiento.EncargadoGestionarEmpresa;
import puntoVenta.PuntoDeVenta;

class ColaboradorRegistroCompraTest {
	private EncargadoRegistrarCompra crc;
	private EncargadoGestionarEmpresa encargadoGestionarEmpresa;
	private PuntoDeVenta punto;
	
	@BeforeEach
	void setUp() throws Exception {
		punto = mock(PuntoDeVenta.class);
		encargadoGestionarEmpresa = mock(EncargadoGestionarEmpresa.class);
		crc = new EncargadoRegistrarCompra();
	}

	@Test
	void testRegistrarCompraCredito() {
		crc.registrarCompraCredito(encargadoGestionarEmpresa, 1139538872, 500, LocalDate.now(), LocalTime.now(), punto);
		assertEquals(1, crc.getRegistroCompras().size());
	}

	@Test
	void testRregistrarCompraEstacionamiento() {
		crc.registrarCompraEstacionamiento("EDB949", 2,LocalDate.now(), LocalTime.now(), punto);
		assertEquals(1, crc.getRegistroCompras().size());
	}
	@Test
	void testPuntoDeControlAcutia() {
		assertEquals(0, crc.getPuntodecontroactual().get());
	}
}
