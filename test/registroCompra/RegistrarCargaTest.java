package registroCompra;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import puntoVenta.PuntoDeVenta;

class RegistrarCargaTest {
	LocalDate fechaActual = LocalDate.now();
	LocalTime horaActual = LocalTime.now();
	PuntoDeVenta punto;
	RegistrarCarga carga;
	@BeforeEach 
	void setUp() throws Exception {
		punto = mock(PuntoDeVenta.class);
		carga = new RegistrarCarga(23, 1139538872, 500, fechaActual, horaActual, punto);
	}
	
 	@Test
	void testGetPuntoVenta() {
		assertEquals(punto, carga.getPuntoVenta());
	}
 	
	@Test
	void testGetNumeroControl() {
		assertEquals(23, carga.getNumeroControl());
	}

	@Test
	void testGetFechaActual() {
		assertEquals(fechaActual, carga.getFechaActual());
	}

	@Test
	void testGetNumeroCelular() {
		assertEquals(1139538872, carga.getNumeroCelular());
	}

	@Test
	void testGetHoraActual() {
		assertEquals(horaActual, carga.getHoraActual());
	}

	@Test
	void testGetMontoACargar() {
		assertEquals(500, carga.getMontoACargar());
	}


}
