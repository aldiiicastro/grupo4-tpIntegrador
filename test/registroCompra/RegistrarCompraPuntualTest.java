package registroCompra;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import puntoVenta.PuntoDeVenta;

class RegistrarCompraPuntualTest {
	LocalDate fechaActual = LocalDate.now();
	LocalTime horaActual = LocalTime.now();
	PuntoDeVenta punto;
	RegistrarCompraPuntual compra;
	@BeforeEach 
	void setUp() throws Exception {
		punto = mock(PuntoDeVenta.class);
		compra = new RegistrarCompraPuntual(23, "EDB949", 3, fechaActual, horaActual, punto);
	}
	
 	@Test
	void testGetPuntoVenta() {
		assertEquals(punto, compra.getPuntoVenta());
	}
 	
	@Test
	void testGetNumeroControl() {
		assertEquals(23, compra.getNumeroControl());
	}

	@Test
	void testGetFechaActual() {
		assertEquals(fechaActual, compra.getFechaActual());
	}

	@Test
	void testGetPatente() {
		assertEquals("EDB949", compra.getPatente());
	}

	@Test
	void testGetHoraActual() {
		assertEquals(horaActual, compra.getHoraActual());
	}

	@Test
	void testGetCantDeHoras() {
		assertEquals(3, compra.getCantDeHoras());
	}

}
