package monitoreoEstacionamiento;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EncagadoGestionarEmpresaTest {
	private EncargadoGestionarEmpresa encargadoGestionarEmpresa;
	private Alerta alerta1;
	private Alerta alerta2;
	private Alerta alerta3;
	private IEmpresa empresa1;
	private IEmpresa empresa2;
	private	IEmpresa empresa3; 
	private List<Alerta> alertas;
	@BeforeEach 
	void setUp() throws Exception {
		//DOC
		empresa1 = mock(IEmpresa.class);
		empresa2 = mock(IEmpresa.class);
		empresa3 = mock(IEmpresa.class);
		alerta1 = mock(Alerta.class);
		alerta2 = mock(Alerta.class);
		alerta3 = mock(Alerta.class);
		alertas = new ArrayList<Alerta>();
		alertas.add(alerta1);
		alertas.add(alerta2);
		alertas.add(alerta3);
		when(alerta1.tipoDeAlerta()).thenReturn(EnumAlerta.Inicio);
		when(alerta2.tipoDeAlerta()).thenReturn(EnumAlerta.Fin);
		when(alerta3.tipoDeAlerta()).thenReturn(EnumAlerta.Carga);
		//SUT
		encargadoGestionarEmpresa = new EncargadoGestionarEmpresa();
		
		//Excercise
		encargadoGestionarEmpresa.agregarUnaAlertaNueva(alerta1);
		encargadoGestionarEmpresa.agregarUnaAlertaNueva(alerta2);
		encargadoGestionarEmpresa.agregarUnaAlertaNueva(alerta3);
		when(empresa1.getAlertas()).thenReturn(alertas);
		when(empresa2.getAlertas()).thenReturn(alertas);
		when(empresa3.getAlertas()).thenReturn(alertas);
		encargadoGestionarEmpresa.suscribirEmpresa(empresa1);
		encargadoGestionarEmpresa.suscribirEmpresa(empresa2);
		encargadoGestionarEmpresa.suscribirEmpresa(empresa3);
	}
	
	@Test
	void testSuscribir() {
		assertEquals(3, encargadoGestionarEmpresa.getEmpresasSuscriptas().size());
	}
	
	@Test
	void testDarDeBaja() {
		assertEquals(3, encargadoGestionarEmpresa.getEmpresasSuscriptas().size());
		encargadoGestionarEmpresa.darDeBaja(empresa3);
		assertEquals(2, encargadoGestionarEmpresa.getEmpresasSuscriptas().size());
	}
	
	@Test
	void testDesuscribir() {
		assertEquals(3, encargadoGestionarEmpresa.empresasSuscriptasA(alerta2).size());
		encargadoGestionarEmpresa.desuscribirEmpresa(empresa1, alerta2);
		assertEquals(2, encargadoGestionarEmpresa.empresasSuscriptasA(alerta2).size());
	}
	
	@Test
	void testNotificar() {
		assertEquals(3, encargadoGestionarEmpresa.empresasSuscriptasA(alerta1).size());
		encargadoGestionarEmpresa.notificarEmpresa(alerta1);
		verify(empresa1, times(1)).actualizar(alerta1);
		verify(empresa2, times(1)).actualizar(alerta1);
		verify(empresa3, times(1)).actualizar(alerta1);
	}
}
