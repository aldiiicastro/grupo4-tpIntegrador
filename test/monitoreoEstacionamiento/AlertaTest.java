package monitoreoEstacionamiento;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import estacionamiento.Estacionamiento;

class AlertaTest {
	private Alerta alerta;
	private Estacionamiento est;
	
	@BeforeEach 
	void setUp() throws Exception {
		//DOC
		est = mock(Estacionamiento.class);
		
		//SUT
		alerta = new Alerta(EnumAlerta.Inicio, est);
	}
	
	@Test
	void testTipoDeAlerta() {
		assertEquals(EnumAlerta.Inicio, alerta.tipoDeAlerta());
	}
	
	@Test
	void testNotificar() {
		assertEquals(est, alerta.notificar());
	}

}
