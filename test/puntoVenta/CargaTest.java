package puntoVenta;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CargaTest {
	private Carga carga;
	
	@BeforeEach
	public void setUp() throws Exception { 
		carga = new Carga(1139538872, 500);
	}
	
	@Test
	void testGetNumero() {
		assertEquals(1139538872, carga.getNumero());
	}
	
	@Test
	void testGetCredito() {
		assertEquals(500, carga.getCredito());
	}
	
	@Test
	void testDescontar() {
		carga.descontar(120);
		assertEquals(380, carga.getCredito());
	}

}
