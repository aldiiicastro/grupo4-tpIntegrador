package puntoVenta;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;

import sem.SEM;

class PuntoDeVentaTest {
	private PuntoDeVenta punto;
	private SEM sem;
	LocalTime horaActual = LocalTime.now();
	LocalDate fechaActual = LocalDate.now();
	@BeforeEach
	public void setUp() throws Exception {  
		//DOC
		sem = mock(SEM.class);
		
		//SUT
		punto = new PuntoDeVenta(sem);
	}
	
	@Test
	void test() {
		punto.pagarEstacionamiento("EDB949", 4, horaActual);

		LocalTime horaInicio = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond());
		LocalTime horaFin =  horaInicio.plusHours(4);
		verify(sem, times(1)).registrarEstacionamiento("EDB949", horaInicio, horaFin,4);
	}
	
	@Test
	void testCargarCredito() {
		punto.cargarCredito(1139538872, 500, horaActual);
		verify(sem, times(1)).registrarCredito(1139538872, 500,fechaActual,horaActual, punto);
	}
}
