package puntoVenta;

import static org.mockito.Mockito.*;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import zona.Zona;

class ViaCompraPuntualTest {
	private Zona zona;
	private CompraPuntual compra;
	private PuntoDeVenta punto;
	@BeforeEach 
	public void setUp() throws Exception {
		//DOC
		zona = mock(Zona.class);
		punto = mock(PuntoDeVenta.class);
		
		//SUT
		compra = new CompraPuntual(zona);
	}
	
	@Test
	void testComprarEstacionamiento() {
		when(zona.obtenerCualquierPunto()).thenReturn(punto);
		LocalTime ahora = LocalTime.now();
		compra.comprarEstacionamiento("EDB949", 4, ahora) ;
		verify(punto, times(1)).pagarEstacionamiento("EDB949", 4, ahora);
	}

}
