package puntoVenta;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ColaboradorCreditoTest {
	private EncargadoGuardarCredito colCre;
	@BeforeEach 
	void setUp() throws Exception {
		//SUT
		colCre = new EncargadoGuardarCredito();
		colCre.registrarCredito(1139538872, 500);
	}
	@Test
	void testCuantoCredito() {
		assertEquals(500, colCre.cuantoCreditoTiene(1139538872));
	}
	
	@Test
	void testCuantoCreditoNoExistente() {
		assertEquals(0, colCre.cuantoCreditoTiene(1139528872));
	}
	
	@Test
	void testDebitarCredito() {
		colCre.debitarCredito(120, 1139538872);
		assertEquals(380, colCre.cuantoCreditoTiene(1139538872));
	}
	
	@Test 
	void testGetCargas() {
		assertEquals(1, colCre.getCargas().size());
	}

}
