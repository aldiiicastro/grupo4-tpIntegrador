package registroCompra;

import java.time.LocalDate;
import java.time.LocalTime;

import puntoVenta.PuntoDeVenta;

public class RegistrarCarga extends RegistroCompra {
	
	private int numeroCelular;
	private int montoACargar;
	
	//Constructor
	public RegistrarCarga(int puntoDeControActual,int numeroCelular,int montoACargar,LocalDate fechaActual,LocalTime horaActual,PuntoDeVenta puntoDeVenta){
		super( puntoDeControActual, fechaActual, horaActual, puntoDeVenta );
		this.montoACargar=montoACargar;
		this.numeroCelular=numeroCelular;
	}
	
	//Getters
	public int getNumeroCelular() {
		return numeroCelular;
	}

	public int getMontoACargar() {
		return montoACargar;
	}


}
