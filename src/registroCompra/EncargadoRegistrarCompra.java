package registroCompra;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import monitoreoEstacionamiento.Alerta;
import monitoreoEstacionamiento.EncargadoGestionarEmpresa;
import monitoreoEstacionamiento.EnumAlerta;
import puntoVenta.PuntoDeVenta;

public class EncargadoRegistrarCompra {
	
	private List<RegistroCompra> registroCompras = new ArrayList<RegistroCompra>();
	private static final AtomicInteger puntoDeControActual = new AtomicInteger(0);  //Es un incrementador provisto por java
	
	//Se registra una carga de credito nueva, y se le avisa a las empresas que hubo una nueva carga
	public void registrarCompraCredito(EncargadoGestionarEmpresa gestionSubs, int numeroCelular,int montoACargar,LocalDate fechaActual, LocalTime horaActual, PuntoDeVenta puntoDeVenta) {
		RegistrarCarga carga = new RegistrarCarga(puntoDeControActual.incrementAndGet(), numeroCelular, montoACargar, fechaActual, horaActual, puntoDeVenta);
		gestionSubs.notificarEmpresa(new Alerta(EnumAlerta.Carga, carga));
		registroCompras.add(carga);
	}
	
	//Se registra una compra de estacioanmiento
	public void registrarCompraEstacionamiento(String patente,int cantDeHoras,LocalDate fechaActual ,LocalTime horaActual, PuntoDeVenta puntoDeVenta) {
		RegistrarCompraPuntual compra = new RegistrarCompraPuntual(puntoDeControActual.incrementAndGet(), patente, cantDeHoras,fechaActual, horaActual, puntoDeVenta);
		registroCompras.add(compra);
	}
	
	//Getters
	public List<RegistroCompra> getRegistroCompras() {
		return registroCompras;
	}

	public AtomicInteger getPuntodecontroactual() {
		return puntoDeControActual;
	}
}
