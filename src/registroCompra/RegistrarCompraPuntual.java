package registroCompra;

import java.time.LocalDate;
import java.time.LocalTime;

import puntoVenta.PuntoDeVenta;

public class RegistrarCompraPuntual extends RegistroCompra {
	private String patente;
	private int cantDeHoras;
	
	//Constructor
	public RegistrarCompraPuntual(int puntoDeControActual, String patente,int cantDeHoras,LocalDate fechaActual,LocalTime horaActual,PuntoDeVenta puntoDeVenta) {
		super( puntoDeControActual, fechaActual, horaActual, puntoDeVenta );
		this.patente=patente;
		this.cantDeHoras=cantDeHoras;
	}

	
	//Getters
	public String getPatente() {
		return patente;
	}

	public int getCantDeHoras() {
		return cantDeHoras;
	}

}
