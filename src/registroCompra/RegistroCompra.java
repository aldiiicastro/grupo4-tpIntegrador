package registroCompra;

import java.time.LocalDate;
import java.time.LocalTime;

import puntoVenta.PuntoDeVenta;

public abstract class RegistroCompra {
	
	protected PuntoDeVenta puntoVenta;
	protected int numeroControl;
	protected LocalDate fechaActual;
	protected LocalTime horaActual;
	
	//Constructor
	public RegistroCompra(int puntoDeControActual,LocalDate fechaActual,LocalTime horaActual,PuntoDeVenta puntoDeVenta){
		this.numeroControl=puntoDeControActual;
		this.fechaActual=fechaActual;
		this.horaActual= horaActual;
		this.puntoVenta=puntoDeVenta;
	}
	
	//Getters
	public PuntoDeVenta getPuntoVenta() {
		return puntoVenta;
	}

	public int getNumeroControl() {
		return numeroControl;
	}

	public LocalDate getFechaActual() {
		return fechaActual;
	}

	public LocalTime getHoraActual() {
		return horaActual;
	}
}
