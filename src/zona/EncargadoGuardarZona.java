package zona;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import inspector.Inspector;
import puntoVenta.PuntoDeVenta;

public class EncargadoGuardarZona {
	private List<Zona> zonas =  new ArrayList<Zona>();
	private int precioXHora = 40;
	private LocalTime finFranja = LocalTime.of(20, 0,0), inicioFranja = LocalTime.of(7, 0, 0);
	
	//Se registra una nueva zona dado una lista de puntos de venta y un inspector
	public void registrar(List<PuntoDeVenta> punto, Inspector inspector) {
		Zona zona = new Zona(punto, inspector);
		inspector.setZonaACargo(zona);
		zonas.add(zona);
		
	}
	//Se obtiene la lista de todas las zonas que exiten en el muncipio
	public List<Zona> getZonas() {
		return zonas;
	}
	
	//Se obtiene el final de la franja horaria
	public LocalTime getFinFranja() {
		return finFranja;
	}

	//Se obtiene el inicio de la franja horaria
	public LocalTime getInicioFranja() {
		return inicioFranja;
	}
	//Se obtiene el precio por hora
	public int getPrecioXHora() {
		return precioXHora;
	}	
	
}
