package zona;

import java.util.ArrayList;
import java.util.List;

import inspector.Inspector;
import puntoVenta.PuntoDeVenta;

public class Zona {
	private List<PuntoDeVenta> puntos = new ArrayList<PuntoDeVenta>();
	private Inspector inspector;

	public Zona(List<PuntoDeVenta> punto2, Inspector inspector2) {
		puntos = punto2;
		inspector = inspector2;
	}
	
	//Se obtienen todos los puntos de venta que estan en la zona
	public List<PuntoDeVenta> getPunto() {
		return puntos;
	}

	//Se obtiene el inspector que revisa la zona
	public Inspector getInspector() {
		return inspector;
	}
		
	//Se obtiene un punto de venta aleatorio
	public PuntoDeVenta obtenerCualquierPunto() {
	    int aleatorio = (int) Math.floor(Math.random()*(puntos.size()));
	    PuntoDeVenta seleccion = puntos.get(aleatorio);
	    return seleccion;
	}
}
