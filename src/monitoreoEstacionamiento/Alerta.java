package monitoreoEstacionamiento;

public  class Alerta {
	protected EnumAlerta tipoDeAlerta;
	protected Object obj;
	
	//Constructor
	public Alerta(EnumAlerta tipoDeAlerta, Object obj) {
		this.obj = obj;
		this.tipoDeAlerta = tipoDeAlerta;
	}
	
	//El tipo de alerta que es
	public EnumAlerta tipoDeAlerta() { //En este momento solo hay 3 tipos de alerta: Inicio, Fin, Carga
		return tipoDeAlerta;	//Cuando las empresas se asocian y les llega una notificacion chequean si el
								//EL tipo de la alerta que les llega es del tipo que buscan 
	}

	//Se notifica que hubo un nuevo objeto.
	public Object notificar() {
		return obj; //Devuelve el objeto, ya que no se sabe que busca exactamente la empresa
	}				//El Objeto puede ser un estacionamiento o una carga por ahora
}
