package monitoreoEstacionamiento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EncargadoGestionarEmpresa implements IObserver {
	protected Map<EnumAlerta, List<IEmpresa>> empresas;

	//Constructor
	public EncargadoGestionarEmpresa() {
		empresas = new HashMap<EnumAlerta, List<IEmpresa>>();
	}
	
	//Se obtiene todas las empresas que se suscribieron
	public Set<IEmpresa> getEmpresasSuscriptas() {
		Set<IEmpresa> empresasSubs = new HashSet<IEmpresa>();
		for (List<IEmpresa> empresasS : empresas.values()) {
			for (int i = 0; i< empresasS.size(); i++) {
				empresasSubs.add(empresasS.get(i));
			}
		}
		return empresasSubs;
	}
	
	//Se suscribe una empresa nueva a las alertas que contiene
	public void suscribirEmpresa(IEmpresa empresa) {
		List<Alerta> alertas = empresa.getAlertas();
		for(Alerta alerta: alertas) {
			suscribirA(empresa, alerta);
		}
	}
	
	private void suscribirA(IEmpresa empresa, Alerta alerta) {
		//Chequeamos si no existe la alerta para que se agregue en el map
		if (!empresas.containsKey(alerta.tipoDeAlerta())) {
			agregarUnaAlertaNueva(alerta);
		}
		//Luego agregamos a la lista de Empresas que contiene esa alerta la empresa dada
		empresasSuscriptasA(alerta).add(empresa);
	}

	//Se desuscribe una empresa de todas las alertas a las que esta susbscripta
	public void darDeBaja(IEmpresa empresa) {
		List<Alerta> alertas = empresa.getAlertas();
		for (Alerta alerta: alertas) {
			empresasSuscriptasA(alerta).remove(empresa);
		}
	}
	
	public void desuscribirEmpresa(IEmpresa empresa, Alerta alerta) {
		empresasSuscriptasA(alerta).remove(empresa);
	}

	//Se notifica a las empresas que hubo una alerta nueva
	public void notificarEmpresa(Alerta alerta) {
		for (IEmpresa empresa : empresasSuscriptasA(alerta)) {
			/*
			 * Hace el notificar por cada una de las empresas que tiene en su lista de asociadas
			 * segun la alerta dada
			 * Se notifican solo a las empresas que corresponden
			 * */
			empresa.actualizar(alerta);
		}
	}
	
	public void agregarUnaAlertaNueva(Alerta alerta) {
		empresas.put(alerta.tipoDeAlerta(), new ArrayList<IEmpresa>());
	}
	
	public List<IEmpresa> empresasSuscriptasA(Alerta alerta) {
		return empresas.get(alerta.tipoDeAlerta());
	}
}
