package monitoreoEstacionamiento;


public interface IObserver {
	/*
	 * Es la interfaz por si mas adelante se necesita agregar otro objeto que tambie pueda suscribir
	 * y desuscribir empresas y notificarlas.
	 * */
	
	public void suscribirEmpresa(IEmpresa empresa);
	
	public void desuscribirEmpresa(IEmpresa empresa, Alerta alerta);
	
	public void darDeBaja(IEmpresa empresa);
	
	public void notificarEmpresa(Alerta alerta);

}
