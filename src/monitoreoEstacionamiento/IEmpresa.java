package monitoreoEstacionamiento;

import java.util.List;

public interface IEmpresa {
	//Todas las empresas deben extender de esta interfaz
	public void actualizar(Alerta alerta);

	public List<Alerta> getAlertas();
	
	//M�todos que usar� cada empresa para manejar las suscripciones
	
	public void suscribir();
	
	
	public void desuscribir(Alerta alerta);
	
	
	public void darDeBaja(); 
}
