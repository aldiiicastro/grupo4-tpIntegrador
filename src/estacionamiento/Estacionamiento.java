package estacionamiento;

import java.time.LocalTime;

public class Estacionamiento {
	private String patente;
	private LocalTime horaInicio, horaFin;
	private int extra;
	
	//El constructor de estacionamiento
	public Estacionamiento(String patente, LocalTime horaInicio2, LocalTime horaFin, int extra) {
		this.patente = patente;
		this.horaInicio = horaInicio2;
		this.horaFin = horaFin;
		this.extra = extra;
	}

	//Se obtiene la patente que se registro a la hora de comprar un estacionamiento
	public String getPatente() {
		return patente;
	}
	
	//Se obtiene la horaFin que se registro a la hora de comprar un estacionamiento
	public LocalTime getHoraFin() {
		return horaFin;
	}
	
	//Se obtiene la horaInicio que se registro a la hora de comprar un estacionamiento
	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	//El extra significa celular o cantidad de horas compradas, en este caso se puso de esta forma
	//Si hubisen sido de tipos distintos hubiesen sido dos constructores distintos
	public int getExtra() {
		return extra;
	}

	//Se puede saber si esta vigente el estacionamiento segun el horario
	public boolean vigencia(LocalTime horaPedido) {
		return horaPedido.isBefore(horaFin); 
	}

	//Cuando se pide que se finalize el estacionamiento, sea por manual, automatico o el operador
	//se cambia la hora que finaliza para que este tenga un nuevo horario de fin.
	public void finDeEstacionamiento(LocalTime finalizado) {
		horaFin = finalizado;
	}
}
