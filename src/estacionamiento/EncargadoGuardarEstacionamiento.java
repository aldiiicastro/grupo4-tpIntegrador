package estacionamiento;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import monitoreoEstacionamiento.Alerta;
import monitoreoEstacionamiento.EncargadoGestionarEmpresa;
import monitoreoEstacionamiento.EnumAlerta;

public class EncargadoGuardarEstacionamiento {
	
	private List<Estacionamiento> estacionamientos = new ArrayList<Estacionamiento>();

	//Se registra un estacionamiento y se notifica a las empresas que se inicio un nuevo estacionamiento.
	public void registrarEstacionamiento(EncargadoGestionarEmpresa gestionSubs, String patente, LocalTime horaInicio, LocalTime horaFin, int segunCompra) {
		Estacionamiento estacionamiento = new Estacionamiento(patente, horaInicio, horaFin, segunCompra);
		gestionSubs.notificarEmpresa(new Alerta(EnumAlerta.Inicio, estacionamiento));
		estacionamientos.add(estacionamiento);
	}
	
	//Se puede saber dada una patente y una hora si el estacionamiento esta vigente
	public boolean estaVigente(String patente, LocalTime ahora) {
		boolean vigencia = false;
		for (Estacionamiento estacionamiento : estacionamientos) {
			if (esLaPantente(estacionamiento, patente)) {
				vigencia = estacionamiento.vigencia(ahora); 
			}
		}
		return vigencia;
	}

	//Se fija que las patente dada y la del estacionamineto sean las mismas
	private boolean esLaPantente(Estacionamiento estacionamiento, String patente) {
		return estacionamiento.getPatente().equals(patente);
	}

	//Se obtiene la lista de todos los estacionamientos que fueron registrados hasta el momento
	public List<Estacionamiento> getEstacionamientos() {
		return estacionamientos;
	}

	//Se finaliza el estacionamiento, segun el celular dado, y se le notifica a 
	//las empresas que se finalizo un estacionamiento
	public void finDeEstacionamiento(EncargadoGestionarEmpresa gestionSubs, LocalTime finalizado, int numeroCelular) {
		for (Estacionamiento estacionamiento : estacionamientos) {
			if (esElCelular(estacionamiento, numeroCelular)) {
				estacionamiento.finDeEstacionamiento(finalizado); 
				gestionSubs.notificarEmpresa(new Alerta(EnumAlerta.Fin, estacionamiento));
			}
		}
	}

	//Se fija que el celular dado y el del estacionamiento sea el mismo.
	//Esto solo vale para los estacionamientos que son registrados en app
	//ya que los demas finalizan a la cantidad de horas que se compro y quedaron registrados
	//desde un primer momento y no deben finalizarse por el sem ni por nadie.
	private boolean esElCelular(Estacionamiento estacionamiento, int numeroCelular) {
		return estacionamiento.getExtra() == numeroCelular;
	}
}
