package puntoVenta;

import java.time.LocalTime;

import zona.Zona;

public class CompraPuntual {
	private Zona zona;
	
	//Constructor
	public CompraPuntual(Zona zona) {
		super();
		this.zona = zona;
	}

	//Se compra un estacionamiento en un punto de venta que el cliente haya elegido
	//La utilidad del obtener cualquier punto en la zona es para que se presente esa 
	//"aletoridad" que puede tener un cliente en el codigo
	public void comprarEstacionamiento(String patente, int cantHoras, LocalTime ahora) {
		zona.obtenerCualquierPunto().pagarEstacionamiento(patente, cantHoras, ahora);
	}
}
