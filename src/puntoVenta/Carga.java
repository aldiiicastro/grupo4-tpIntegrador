package puntoVenta;

public class Carga {
	private int numTel, monto;

	//Constructor
	public Carga(int numTel, int monto) {
		super();
		this.numTel = numTel;
		this.monto = monto;
	}

	//Se obtiene el numero de telefono
	public int getNumero() {
		return numTel;
	}

	//Se obtiene el credito
	public int getCredito() {
		return monto;
	}

	//Se le descuenta cierto monto al credito
	public void descontar(int costo) {
		monto -= costo;
	}
	
}
