package puntoVenta;

import java.util.ArrayList;
import java.util.List;

public class EncargadoGuardarCredito {
	private List<Carga> cargas = new ArrayList<Carga>(); 
	
	//Se registra una carga de credito nueva
	public void registrarCredito(int numeroCelular, int montoACargar) {
		Carga carga = new Carga(numeroCelular, montoACargar);
		cargas.add(carga);
	}

	//Dado un celular se puede saber cuanto credito tiene
	public int cuantoCreditoTiene(int numeroCel) {
		for (Carga carga : cargas) {
			if (carga.getNumero() == numeroCel) {
				return carga.getCredito();
			}
		}
		return 0;
	}

	//Dado un celular y un costo se le pude descontar el credito
	public void debitarCredito(int costo, int numeroCelular) {
		for (Carga carga : cargas) {
			if (carga.getNumero() == numeroCelular) {
				carga.descontar(costo);
			}
		}
	}

	//Se obtiene una lista de todas las cargar hechas hasta el momento
	public List<Carga> getCargas() {
		return cargas;
	}


}
