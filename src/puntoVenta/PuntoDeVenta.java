package puntoVenta;

import java.time.LocalDate;
import java.time.LocalTime;

import sem.SEM;

public class PuntoDeVenta {
	private SEM sem;
	private LocalTime horaInicio ;
	private LocalDate fechaActual = LocalDate.now(); 
	
	//Constructor
	public PuntoDeVenta(SEM sem) {
		super();
		this.sem = sem;
	}
	
	//Le pide al sem que registre una nueva carga de credito al numero de celular dado
	public void cargarCredito(int numeroCelular, int montoACargar, LocalTime horaActual) {
		sem.registrarCredito(numeroCelular, montoACargar, fechaActual, horaActual, this);
	}
	
	//Se paga el estacionamiento presencialmente, entonces se le avisa al sem que hay 
	//una nueva compra y registro de estacionamiento
	public void pagarEstacionamiento(String patente, int cantDeHoras, LocalTime horaActual) {
		horaInicio = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond());
		LocalTime horaFin = calcularHoraFin(cantDeHoras);
		sem.registrarEstacionamiento(patente, horaInicio, horaFin, cantDeHoras);
		sem.registrarCompraEstacionamiento(patente, cantDeHoras,fechaActual, horaActual, this);
	}

	//Se calcula la hora que el estacionamiento debe terminar.
	private LocalTime calcularHoraFin(int cantDeHoras) {
		LocalTime horaFin =  horaInicio.plusHours(cantDeHoras);
		return horaFin;
	}
}
