package sem;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import appDeUsuario.EncargadoAdminstrarApp;
import estacionamiento.EncargadoGuardarEstacionamiento;
import estacionamiento.Estacionamiento;
import inspector.EncargadoGuardarInfraccion;
import inspector.Inspector;
import monitoreoEstacionamiento.Alerta;
import monitoreoEstacionamiento.EncargadoGestionarEmpresa;
import monitoreoEstacionamiento.IEmpresa;
import puntoVenta.EncargadoGuardarCredito;
import puntoVenta.PuntoDeVenta;
import registroCompra.EncargadoRegistrarCompra;
import zona.EncargadoGuardarZona;

public class SEM {
	
	private EncargadoGuardarInfraccion encargadoGuardarInfraccion;
	private EncargadoGuardarCredito encargadoGuardarCredito;
	private EncargadoGuardarEstacionamiento encargadoGuardarEstacionamiento;
	private EncargadoAdminstrarApp encargadoAdministrarApp;
	private EncargadoGuardarZona encargadoGuardarZona;
	private EncargadoRegistrarCompra encargadoRegistrarCompra;
	private EncargadoGestionarEmpresa encargadoGestionarEmpresa;
	
	//Se crea el sem con todos sus colaboradores internos.
	public SEM(EncargadoGuardarInfraccion encargadoGuardarInfraccion, 
			EncargadoGuardarCredito encargadoGuardarCredito,
			EncargadoGuardarEstacionamiento encargadoGuardarEstacionamiento, 
			EncargadoAdminstrarApp encargadoAdminstrarApp,
			EncargadoGuardarZona encargadoGuardarZona, 
			EncargadoRegistrarCompra engargadoRegistrarCompra,
			EncargadoGestionarEmpresa encargadoGestionarEmpresa) {
		super();
		this.encargadoGuardarInfraccion = encargadoGuardarInfraccion;
		this.encargadoGuardarCredito = encargadoGuardarCredito;
		this.encargadoGuardarEstacionamiento = encargadoGuardarEstacionamiento;
		this.encargadoAdministrarApp = encargadoAdminstrarApp;
		this.encargadoGuardarZona = encargadoGuardarZona;
		this.encargadoRegistrarCompra = engargadoRegistrarCompra;
		this.encargadoGestionarEmpresa = encargadoGestionarEmpresa;
	}

	//El sem le pide a su colaborador interno, encargado guardar zona, que registre una zona.
	public void registrarZona(List<PuntoDeVenta> punto, Inspector inspector) {
		encargadoGuardarZona.registrar(punto, inspector);
	}
	
	//El sem le pide a su colaborador interno, encargadoRegistrarCompra, que registre una 
	//nueva compra de estacionamiento
	public void registrarCompraEstacionamiento(String patente, int cantDeHoras,LocalDate fechaActual,
			LocalTime horaActual, PuntoDeVenta puntoDeVenta) {
			encargadoRegistrarCompra.registrarCompraEstacionamiento(patente, cantDeHoras,fechaActual, horaActual, puntoDeVenta);
	}
	
	//El sem le pide a su colaborador interno, encargado guardar estacionamiento, que registre un estacionamiento
	//nuevo
	public void registrarEstacionamiento(String patente, LocalTime horaInicio, LocalTime fin, int ext) {
		encargadoGuardarEstacionamiento.registrarEstacionamiento(encargadoGestionarEmpresa, patente, horaInicio, fin, ext);
	}
	
	//El sem le pide a su colaborador interno, encargado registrar compra, que registre una compra de credito
	//El sem le pide a su colaborador interno, encargado guardar credito, que registre el credito cargado.
	public void registrarCredito(int numeroCelular, int montoACargar,LocalDate fechaActual, LocalTime horaActual, PuntoDeVenta punto) {
		encargadoRegistrarCompra.registrarCompraCredito(encargadoGestionarEmpresa, numeroCelular, montoACargar,fechaActual, horaActual, punto);
		encargadoGuardarCredito.registrarCredito(numeroCelular, montoACargar);
	}

	//El sem le pide a su colaborador interno, encargado de guardar infraccion, que guarde la nueva infraccion
	public void registrarInfraccion(String patente, Inspector inspector, LocalTime soloHora, LocalDate soloFecha) {
		encargadoGuardarInfraccion.guardarInfraccion(patente, inspector, soloHora, soloFecha);
	}
	
	
	//Se finaliza la franja horaria una vez que se llego a las 8 de la noche
	public String finDeFranjaHoraria(LocalTime fin) {
		List<Estacionamiento> est = encargadoGuardarEstacionamiento.getEstacionamientos();
		if (encargadoGuardarZona.getFinFranja().equals(fin)) {
			for (Estacionamiento estacionamiento : est) {
				if (estacionamiento.vigencia(fin)) {
					finDeEstacionamiento(fin, estacionamiento.getExtra());
				}
			}
			return "Termino el horario";
		}
		else {
			return "Aun no termino el horario";
		}
	}
	
	//El sem le pide a su colaborador interno, encargado de gestionar empresa, que suscriba a la empresa dada a una lista de alertas
	public void suscribirEmpresa(IEmpresa empresa) {
		encargadoGestionarEmpresa.suscribirEmpresa(empresa);
	}
	
	//El sem le pide a su colaborador interno, encargado de gestionar empresa, que desuscriba a la empresa dada segun la alerta dada	
	public void desuscribirEmpresa(IEmpresa empresa, Alerta alerta) {
		encargadoGestionarEmpresa.desuscribirEmpresa(empresa, alerta);
	}
	
	//El sem le pide a su colaborador interno, encargado de gerstionar empresa, que de debaja a la empresa
	public void darDeBaja(IEmpresa empresa) {
		encargadoGestionarEmpresa.darDeBaja(empresa);
	}
	
	//Metodos de ayuda entre colaboradores;
	//Finalizar e iniciar estacionamiento son llamados por la AppUsuario
	//El sem le pide a su colaborador interno, encargado administrar app que inicialize un estacionamiento;
	public String iniciarEstacionamineto(LocalTime ahora, String patente, int numeroCelular) {
		return encargadoAdministrarApp.iniciarEstacionamineto(patente, numeroCelular, ahora);
	}
	
	//El sem le pide a su colaborador interno, encargado administrar app que finalize un estacionamiento;
	//Y a encargado guardar estacionamiento que cambie el horario de fin para finalizar el estacionamiento.
	public String finDeEstacionamiento(LocalTime finalizar, int numeroCelular) {
		encargadoGuardarEstacionamiento.finDeEstacionamiento(encargadoGestionarEmpresa, finalizar, numeroCelular);
		return encargadoAdministrarApp.finDeEstacionamiento(finalizar, numeroCelular);
	}

	//Se puede consultar el saldo del numero de celular dado
	public int consultarSaldo(int numeroCel) {
		return encargadoGuardarCredito.cuantoCreditoTiene(numeroCel);
	}

	//Se puede saber si dada una patente y una hora si ese estacionamiento esta vigente
	public boolean estaVigente(String patente, LocalTime ahora) {
		return encargadoGuardarEstacionamiento.estaVigente(patente, ahora);
	}

	public Set<IEmpresa> getEmpresasSuscriptas() {
		return encargadoGestionarEmpresa.getEmpresasSuscriptas();
	}
	
}
