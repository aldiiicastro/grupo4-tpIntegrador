package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;

public interface Estado {

	//Para hacer lo que corresponde cuando esta caminando, recibe al sem para poder avisarle, si corresponde
 	public void caminando(SEM sem, LocalTime ahora);

 	//Para hacer lo que corresponde cuando esta manejando, recibe al sem para poder avisarle, si corresponde
 	public void manejando(SEM sem,  LocalTime ahora);
 	
 	//Retorna el mensaje que se recibio
 	public String getMsg();
 	
 	//Se cambia a modo caminando 
 	public void cambiarACamiando(AppUsuarioAutomatica app);
 	
 	//Se cambia a modo manjando
 	public void cambiarAManejando(AppUsuarioAutomatica app);

}
