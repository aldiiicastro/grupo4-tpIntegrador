package appDeUsuario;

import java.time.LocalTime;

public interface IStrategy  extends MovementSensor{
	//Se inicia un estacionamiento
	public String iniciarEstacionamiento(LocalTime ahora);
	
	//Se finaliza un estacionamiento
	public String finDeEstacionamiento(LocalTime ahora);
	
	//Se notifica segun el moment sensor
	public String notificaciones();

	public int consultaSaldo(int celular);

}
