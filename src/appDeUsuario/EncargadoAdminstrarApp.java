package appDeUsuario;

import java.time.LocalTime;
import puntoVenta.EncargadoGuardarCredito;
import sem.SEM;

public class EncargadoAdminstrarApp {
	private LocalTime horaInicio, franjaInicio, franjaFin;
	private int credito, precioXHora;
	private EncargadoGuardarCredito colaboradorCredito; 
	private SEM sem;
	
	//Constructor
	public EncargadoAdminstrarApp(int precioXHora, LocalTime franjaInicio, LocalTime franjaFin, EncargadoGuardarCredito colaboradorCredito,
			SEM sem) {
		this.precioXHora = precioXHora;
		this.franjaInicio = franjaInicio;
		this.franjaFin = franjaFin;
		this.colaboradorCredito = colaboradorCredito;
		this.sem = sem;
	}

	//Se inicializa un estacionamiento, para inicializarlo se tiene que fijar si corresponde por horario
	//Si el credito que tiene le es suficiente
	//Si se puede inicializar, se le pide al sem que lo registre
	public String iniciarEstacionamineto(String patente, int celular, LocalTime horaActual) {
		credito = colaboradorCredito.cuantoCreditoTiene(celular);
		horaInicio = LocalTime.of(horaActual.getHour(), horaActual.getMinute() , horaActual.getSecond());
		LocalTime fin = calcularFin(celular);
		String msg = "";
		if (noEsHoraDeCobro()) { 
			msg = "No esta dentro del horario de cobro";
		} else if (credito >= precioXHora) {
			sem.registrarEstacionamiento(patente, horaInicio, fin, celular);
			msg = "Arranca estacionamiento a las: " + horaInicio + " tiene tiempo hasta las: " + fin;
		} else {
			msg = "Saldo insuficiente. Estacionamiento no permitido";
		}
		return msg;
	}
	
	//Se fija que no sea la hora de cobro
	private boolean noEsHoraDeCobro() {
		return horaInicio.isBefore(franjaInicio) || horaInicio.isAfter(franjaFin);
	}

	//Se calcula el fin del estaciomiento
	private LocalTime calcularFin(int celular) {
		long cantHoras = credito / precioXHora;
		if (horaInicio.isAfter(franjaFin)) {
			return franjaFin;
		} else {
			return horaInicio.plusHours(cantHoras);
		}
	}

	//Se finaliza el estacionamiento, para esto hay que descontrarl el credito correspondiente.
	public String finDeEstacionamiento(LocalTime finalizar, int numeroCelular) {
		int totalDeHoras = finalizar.minusHours(horaInicio.getHour()).getHour();
		int costo = totalDeHoras * precioXHora;
		colaboradorCredito.debitarCredito(costo, numeroCelular);
		return "Comenzo a las: " + horaInicio + "finalizo a las: " + finalizar + ", un total de: " + totalDeHoras + " horas. Costo: " + costo;
	}
}
