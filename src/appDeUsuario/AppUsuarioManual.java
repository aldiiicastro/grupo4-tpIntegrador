package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;

public class AppUsuarioManual implements IStrategy{
	private int celular;
	private String patente, msg = "";
	private SEM sem;
	
	//Constructor
	public AppUsuarioManual(int celular, String patente, SEM sem) {
		this.patente = patente;
		this.celular = celular;
		this.sem = sem;
	}
	
	//Se le pide al sem que inicialize un estacionamiento
	public String iniciarEstacionamiento(LocalTime ahora) {
		return sem.iniciarEstacionamineto(ahora, patente, celular);
	}
	
	//Se le pide al sem que finalize un estacionamiento
	public String finDeEstacionamiento(LocalTime ahora) {
		return sem.finDeEstacionamiento(ahora, celular);
	}

	//Segun el moment sensor se guarda el mensaje para notificarlo
	@Override
	public void driving() {
		msg = "Esta manejando, no se olvide de finalizar el estacionamiento";
	}

	//Segun el moment sensor se guarda el mensaje para notificarlo
	@Override
	public void walking() {
		msg = "Esta caminando, no se olvide de inicializar el estacionamiento";
	}
	
	//Getters 
	@Override
	public String notificaciones() {
		return msg;
	}

	public int consultaSaldo(int celular) {
		return sem.consultarSaldo(celular);
	}
	
	public int nroCelular() {
		return celular;
	}
	
	public String obtenerPatente() {
			return patente;
	}
}
