package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;

public class AppUsuarioAutomatica implements IStrategy {
	private Estado estado;
	private int celular;
	private String patente, msg = "";
	private SEM sem;	
	
	//Constructor
	public AppUsuarioAutomatica(int celular, String patente, SEM sem) {
		this.patente = patente;
		this.celular = celular;
		this.sem = sem;
		estado = new EstadoCaminando(patente, celular);
	}
	
	//Segun el estado en que se encuentra se inicializa un estacionamiento
	@Override
	public String iniciarEstacionamiento(LocalTime ahora) {
		estado.caminando(this.sem, ahora);
		return estado.getMsg();
	}

	//Segun el estado en que se encuentra se finaliza un estacionamiento
	@Override
	public String finDeEstacionamiento(LocalTime ahora) {
		estado.manejando(this.sem, ahora);
		return estado.getMsg(); 
	}

	//Segun el moment sensor se guarda un mensaje, ademas se cambia el estado
	//Como esta Manejando se finaliza
	@Override
	public void driving() {
		estado.cambiarAManejando(this);
		finDeEstacionamiento(LocalTime.now());
		msg = "Esta manejando, automaticamente se finaliza el estacionamiento";
	}

	//Segun el moment sensor se guarda un mensaje, ademas se cambia el estado
	//Como esta caminando se inizializa
	@Override
	public void walking() {
		estado.cambiarACamiando(this);
		iniciarEstacionamiento(LocalTime.now());
		msg = "Esta caminando, automaticamente se inicializa el estacionamiento";
	}
	
	//Getters
	@Override
	public String notificaciones() {
		return msg;
	}
	
	public int consultaSaldo(int celular) {
		return sem.consultarSaldo(celular);
	}
	
	public int nroCelular() {
		return celular;
	}
	
	public String obtenerPatente() {
			return patente;
	}
	
	public Estado getEstado() {
		return estado;
	}
	//Setter
	public void setEstado(Estado e) {
		this.estado = e;
	}
}
