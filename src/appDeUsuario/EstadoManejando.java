package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;

public class EstadoManejando implements Estado{
	private int celular;
	private String msg;

	public EstadoManejando(int celular) {
		this.celular = celular;
	}

	@Override
	public void caminando(SEM sem, LocalTime ahora) {
		//No corresponde
		msg = "Esta manejando no se puede inicializar";
	}

	@Override
	public void manejando(SEM sem, LocalTime ahora) {
		setMsg(sem.finDeEstacionamiento(ahora, celular));
	}
	
	@Override
	public void cambiarAManejando(AppUsuarioAutomatica app) {
		setCelular(app.nroCelular());
		app.setEstado(this);
	}
	
	
	@Override
	public void cambiarACamiando(AppUsuarioAutomatica app) {
		String patente = app.obtenerPatente();
		app.setEstado(new EstadoCaminando(patente, app.nroCelular()));
	}
	
	//Getters y setters
	public String getMsg() {
		return msg;
	}

	public int getCelular() {
		return celular;
	}

	private void setCelular(int celular) {
		this.celular = celular;
	}

	private void setMsg(String msg) {
		this.msg = msg;
	}
	
}
