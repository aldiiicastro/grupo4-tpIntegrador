package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;


public class AppUsuario {
	private int celular;
	private String patente;
	private SEM sem;
	private IStrategy strategy;
	
	public AppUsuario(int celular, String patente, SEM sem) {
		this.sem = sem;
		this.celular = celular;
		this.patente = patente;
		modoManual();
	}

	//Se inicializa el estacinamiento segun si es Manual o Automatica
	public String iniciarEstacionamiento(LocalTime ahora) {
		return strategy.iniciarEstacionamiento(ahora);
	}
	
	//Se finaliza el estacionamiento segun si es Manual o Automatica
	public String finDeEstacionamiento(LocalTime ahora) {
		return strategy.finDeEstacionamiento(ahora);
	}
	
	//Se notifica segun el sensor de movimiento
	public String getNotificaciones() {
		return strategy.notificaciones();
	}
	
	//Para que el usuario elija si la quiere en modo automatico
	public void modoAutomatico() {
		setStrategy(new AppUsuarioAutomatica(celular, patente, sem));
	}
	
	//Para que el usuario elija si la quiere en modo manual
	public void modoManual() {
		setStrategy(new AppUsuarioManual(celular, patente, sem));
	}
	
	//Getters y setters
	public int consultaSaldo(int celular) {
		return strategy.consultaSaldo(celular);
	}
	
	public int nroCelular() {
		return celular;
	}
	
	public String obtenerPatente() {
		return patente;
	}
	
	public IStrategy getStrategy() {
		return strategy; 
	}
	
	private void setStrategy(IStrategy strategy) {
		this.strategy = strategy; 
	}
}
