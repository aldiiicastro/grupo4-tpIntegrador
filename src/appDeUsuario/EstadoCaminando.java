package appDeUsuario;

import java.time.LocalTime;

import sem.SEM;

public class EstadoCaminando implements Estado{
	private String patente, msg;
	private int celular;

	//Constructor
	public EstadoCaminando(String patente, int celular) {
		this.patente = patente;
		this.celular = celular;
	}
	
	//Se cambia al modo caminando, que es este mismo estado,
	//Para eso se setea la patente y el celular dado la app
	@Override
	public void cambiarACamiando(AppUsuarioAutomatica app) {
		setPatente(app.obtenerPatente());
		setCelular(app.nroCelular());
		app.setEstado(this);
	}
	
	//Se cambia a modo manejando, que es un estado nuevo, y para eso se le pide 
	//a la app el celular asi se puede crear
	@Override
	public void cambiarAManejando(AppUsuarioAutomatica app) {
		app.setEstado(new EstadoManejando(app.nroCelular()));
	}
	
	//Se le avisa al sem que hay un estacionamiento nuevo
	@Override
	public void caminando(SEM sem, LocalTime ahora) {
		setMsg(sem.iniciarEstacionamineto(ahora, patente, celular));
	}

	//No corresponde
	@Override
	public void manejando(SEM sem, LocalTime ahora) {
		//No corresponde
		msg = "Esta caminando, no se puede finalizar";
	}
	
	//Getters y setters
	public String getMsg() {
		return msg;
	}

	public String getPatente() {
		return patente;
	}
	
	public int getCelular() {
		return celular;
	}
	
	private void setPatente(String patente) {
		this.patente = patente;
	}

	private void setCelular(int celular) {
		this.celular = celular;
	}

	private void setMsg(String msg) {
		this.msg = msg;
	}

}
