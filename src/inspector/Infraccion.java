package inspector;

import java.time.LocalDate;
import java.time.LocalTime;

import zona.Zona;

public class Infraccion {
	private Inspector inspector;
	private Zona zona;
	private String patente; 
	private LocalDate fecha;
	private LocalTime hora;
	
	public Infraccion(String patente, Inspector inspector, LocalTime soloHora, LocalDate soloFecha) {
		super();
		hora = soloHora;
		fecha = soloFecha;
		this.patente = patente;
		this.inspector = inspector;
		this.zona = inspector.getZonaACargo();
	}

	//Getters
	public Inspector getInspector() {
		return inspector;
	}

	public Zona getZona() {
		return zona;
	}

	public String getPatente() {
		return patente;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public LocalTime getHora() {
		return hora;
	}

	
}
