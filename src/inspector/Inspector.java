package inspector;

import java.time.LocalDate;
import java.time.LocalTime;

import zona.Zona;

public class Inspector {
	private Zona zonaACargo;
	private AppMunicipal app;
	
	//Constructor
	public Inspector(AppMunicipal app) {
		super();
		this.app = app;
	}

	//Se obtiene la zona a cargo de vigilar
	public Zona getZonaACargo() {
		return zonaACargo;
	}

	//Se setea la zona cargo a vigilar
	public void setZonaACargo(Zona zonaACargo) {
		this.zonaACargo = zonaACargo;
	}
	
	//Se fija en su app si la patente esta vigente
	public boolean estaVigenteEstacionamiento(String patente, LocalTime ahora) {
		return app.estaVigente(patente, ahora);
	}
	
	//Crea una infraccion cuando la patente no esta vigente.
	public void altaDeInfraccion(String patente, LocalTime ahora) {
		if (!estaVigenteEstacionamiento(patente, ahora)) {
			LocalDate soloFecha = LocalDate.now();
			app.infraccion(patente,this, ahora, soloFecha);
		}
	}
	
}
