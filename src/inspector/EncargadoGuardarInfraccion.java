package inspector;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
public class EncargadoGuardarInfraccion {
	private List<Infraccion> infracciones = new ArrayList<Infraccion>();

	//Getter de la lista de infracciones labradas hasta el momento pedido
	public List<Infraccion> getInfracciones() {
		return infracciones;
	}

	//Se guarda una nueva infraccion que pidio el inspector
	public void guardarInfraccion(String patente, Inspector inspector, LocalTime soloHora, LocalDate soloFecha) {
		Infraccion infraccion = new Infraccion(patente, inspector, soloHora, soloFecha);
		infracciones.add(infraccion);
	}

}
