package inspector;

import java.time.LocalDate;
import java.time.LocalTime;

import sem.SEM;
public class AppMunicipal {
	private SEM sem;

	//Constructor
	public AppMunicipal(SEM sem2) {
		sem = sem2;
	}

	//Le pide al sem saber si esta vigente una patente dada segun la hora
	public boolean estaVigente(String patente, LocalTime ahora) {
		return sem.estaVigente(patente, ahora);
	}

	///Le avisa al sem que tiene que registrar una nueva infraccion
	public void infraccion(String patente,Inspector inspector, LocalTime soloHora, LocalDate soloFecha) {
		sem.registrarInfraccion(patente,inspector, soloHora, soloFecha);
	}
	
	
}
